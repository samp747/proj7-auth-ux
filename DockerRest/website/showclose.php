<html>
    <head>
        <title>Brevet Event Checkpoint Display</title>
    </head>

    <body>
        <h1>List of checkpoints: Only Closing Times</h1>
        <ul>
            <?php
            $json = file_get_contents('http://api-service/listCloseOnly/json');
            $obj = json_decode($json);
	    $checkpoints = $obj->checkpoints;
	    foreach ($checkpoints as $c) {
		    $km = $c->km;
		    $close = $c->close;
                echo "<li><b>$km km Checkpoint:</b> Closes: $close</li>";
            }
            ?>
        </ul>
    </body>
</html>
