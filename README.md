Name: Sam Peters
Email: speters8@uoregon.edu

RUSA ACP controle time calculator implemented with flask and ajax.

Credits to Michal Young for the initial version of this code.

## API Information

* The API is running in the api-service container, and accesible through port 5001.

* Each endpoint that returns json data uses a list of dictionaries to represent the checkpoint info, where each dictionary represents one checkpoint. Each dictionary contains 2-3 key-value pairs. The first one (key is 'km') represents the checkpoint's location and is always included, while the latter 1-2 represent the open time (key of 'open') or the close time (key of 'close') or both depending on the endpoint.

* Endpoints that return CSV data follow a similar format. With returned CSV data, every row represents one checkpoint, and each row consists of 2-3 values, with the first value representing the checkpoints location in km, and the next 1-2 either representing the open time or close time, or both depending on the endpoint used.

* Both types of endpoints should return checkpoints in ascending order. I interpreted this as meaning the checkpoints should be in order from earliest in the brevet/earliest opening to latest opening.

## API Consumer Program Information

* Consumer program is running in the website container, and accesible through port 5003.

* The program has several different webpages created with php, with each page querying a different api endpoint. They are:
    * /showall.php . This displays all the checkpoints along with their opening and closing times. Uses the /listAll api endpoint.
    * /showfew.php . This displays just the first two checkpoints' opening and closing times. Uses the /listAll api endpoint along with the 'top' query parameter.
    * /showclose.php . This displays all the checkpoints along with just their closing times. Uses the /listOnlyClose/json api endpoint.

## Testing the Submit and Display buttons
I created a separated file, SubmitTesting.txt that describes manual test cases that can be used to check if the submit and display buttons work with all kinds of cases.
It can be found in the DockerMongo folder.

## How I calculated opening and closing times:

I based all my calculations on the algorithms described on this page:https://rusa.org/pages/acp-brevet-control-times-calculator
Any parts that were still ambiguous I based on the Randonneurs USA's Rules for Organizers page, linked here: https://rusa.org/pages/orgreg

My logic splits the checkpoints into 3 types: opening checkpoints, intermediate checkpoints, and closing checkpoints. Opening checkpoints are any checkpoint that is 0 km from the brevet start. Closing checkpoints are any checkpoints that are at or beyond the brevets official distance. Any other checkpoints are classified as intermediate checkpoints.

Opening checkpoints:
	Opening time: At brevet start time.
	Closing time: One hour after brevet start time.

Intermediate checkpoints: Opening times and closing times are calculated using the algorithm found here: https://rusa.org/pages/acp-brevet-control-times-calculator

Closing checkpoints:
	Opening time: Using the same algorithm as the intermediate checkpoints times, except that the calculation is done using the brevets official distance, instead of the checkpoint distance.
	Closing time: A set amount of time after the brevet start time, based on brevet length. 13 hours and 30 minutes for 200km brevets, 20 hours for 300km, 27 hours for 400km, 40 hours for 600 km, and 75 hours for 1000 km brevets.

## Testing

* To test the program rune the command "nosetests" from within the docker container. To do this I used the command "docker exec -it flask nosetests" because my run script automatically names the container running this program flask. 

* Because acp_times.py uses a python module that doesn't come with python by default, any testing of acp_times should only be done from within the docker container

* I implemented functionality that checks for invalid input in the webpage's javascript, so any testing I do of acp_times.py assumes that invalid input will be filtered out before it gets to the open_time() and close_time() functions

## Other notes:

* In order for a time to be calculated, the web page page requires that you submit a valid distance in the mile or kilometer fields for a checkpoint. If the input cannot be interpreted as a non-negative int, no calculations will happen and the page will not update. Similarly, if a checkpoints distance from the start is 20% more than the official brevet distance, it will also be viewed as invalid and no calculations will happen.

* When working on this program, I used the bash script run_dev.sh to stop the old version of the program and creating/started a new docker container.  

* When calculating times for non integer distances, the nearest integer is used instead of the actual distance. This is based off an example brevet on the rusa.org site.
